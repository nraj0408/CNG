ARG GITLAB_BASE_IMAGE=

FROM ${GITLAB_BASE_IMAGE} as build

ARG GITLAB_USER=git
ARG UID=1000

ADD gitlab-shell.tar.gz /assets/
ADD gitlab-logger.tar.gz /assets/usr/local/bin
ADD gitlab-gomplate.tar.gz /assets/

COPY scripts/ /assets/scripts/
COPY sshd_config /assets/etc/ssh/

RUN mkdir -p /assets/srv/sshd /assets/var/log/gitlab-shell \
    && touch /assets/var/log/gitlab-shell/gitlab-shell.log \
    && chmod -R g=u /assets/srv/sshd /assets/srv/gitlab-shell /assets/var/log/gitlab-shell /assets/etc/ssh /assets/scripts

## FINAL IMAGE ##

FROM ${GITLAB_BASE_IMAGE}

ARG GITLAB_USER=git
ARG UID=1000

ARG GITLAB_SHELL_VERSION
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-shell" \
      name="GitLab Shell" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_SHELL_VERSION} \
      release=${GITLAB_SHELL_VERSION} \
      summary="SSH access and repository management app for GitLab." \
      description="SSH access and repository management app for GitLab."

# IMPORTANT: --chown UID:0, or all will be 0:0
COPY --from=build --chown=${UID}:0 /assets/ /

# install runtime deps, remove files overwritten in runtime, add user
RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 procps fipscheck-lib openssh openssh-server \
    && microdnf clean all \
    && rm -r /usr/local/tmp /usr/libexec/openssh/ssh-keysign \
    && rm -rf /etc/ssh/ssh_host_* /etc/ssh/sshd_config.* \
    && chown ${UID}:0 /etc/ssh \
    && install -o 0 -g 0 -m 0755 /scripts/authorized_keys /authorized_keys \
    && install -d -o ${UID} -g 0 -m 770 /home/${GITLAB_USER} \
    && adduser -M -d /home/${GITLAB_USER} -u ${UID} ${GITLAB_USER}

ENV CONFIG_TEMPLATE_DIRECTORY=/srv/gitlab-shell

USER ${UID}

CMD ["/scripts/process-wrapper"]

VOLUME /var/log/gitlab-shell

HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD /scripts/healthcheck
