## FINAL IMAGE ##

ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal:8.7

FROM ${UBI_IMAGE}

ARG REGISTRY_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-container-registry" \
      name="Container Registry" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${REGISTRY_VERSION} \
      release=${REGISTRY_VERSION} \
      summary="The Docker toolset to pack, ship, store, and deliver content." \
      description="The Docker toolset to pack, ship, store, and deliver content. This is a fork of official Docker Registry 2.0 implementation."

ADD gitlab-gomplate.tar.gz /
ADD gitlab-container-registry.tar.gz /

COPY scripts/ /scripts/

RUN microdnf update -y && \
    microdnf ${DNF_OPTS} install --best --assumeyes --setopt=install_weak_deps=0 shadow-utils && \
    microdnf ${DNF_OPTS} reinstall --nodocs --best --assumeyes --setopt=install_weak_deps=0 tzdata -y && \
    adduser -m ${GITLAB_USER} -u ${UID} && \
    ln -sf /usr/local/bin/registry /bin/registry && \
    chgrp -R 0 /scripts /home/${GITLAB_USER} && \
    chmod -R g=u /scripts /home/${GITLAB_USER} && \
    # remove shadow-utils within the same layer
    microdnf remove shadow-utils && \
    microdnf clean all

USER ${UID}

ENV CONFIG_DIRECTORY=/etc/docker/registry
ENV CONFIG_FILENAME=config.yml
ENV GITLAB_USER=${GITLAB_USER}

ENTRYPOINT ["/scripts/entrypoint-ubi8.sh"]
CMD ["/scripts/process-wrapper"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
