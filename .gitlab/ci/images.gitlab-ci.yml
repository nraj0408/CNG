include:
  # debian based images
  - local: .gitlab/ci/debian.gitlab-ci.yml
    rules:
      - if: '$UBI_PIPELINE != "true" && $CI_COMMIT_TAG !~ /^v\d+\.\d+\.\d+(-rc\d+)?-ubi8$/ && $CI_COMMIT_REF_NAME !~ /-ubi8$/ && $FIPS_PIPELINE != "true" && $CI_COMMIT_TAG !~ /^v\d+\.\d+\.\d+(-rc\d+)?-fips$/ && $CI_COMMIT_REF_NAME !~ /-fips$/'
  # ubi based images
  - local: .gitlab/ci/ubi.gitlab-ci.yml
    rules:
      - if: '$UBI_PIPELINE == "true" || $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(-rc\d+)?-ubi8$/ || $CI_COMMIT_REF_NAME =~ /-ubi8$/'
  # fips based images
  - local: .gitlab/ci/fips.gitlab-ci.yml
    rules:
      - if: '$FIPS_PIPELINE == "true" || $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(-rc\d+)?-fips$/ || $CI_COMMIT_REF_NAME =~ /-fips$/'

# Common jobs for image pipelines

trigger-chart-test:
  stage: final-list
  trigger:
    project: 'gitlab-org/charts/gitlab'
    branch: master-trigger-branch
    strategy: depend
  inherit:
    variables: false
  variables:
    REVIEW_REF_PREFIX: "${CI_PIPELINE_ID}-"
  rules:
    # Run only on Canonical and security mirror
    - if: '$CI_PROJECT_PATH != "gitlab-org/build/CNG" && $CI_PROJECT_PATH != "gitlab-org/security/charts/components/images"'
      when: never
    # For UBI pipelines, the images has a `-ubi8` suffix
    - if: '$CI_COMMIT_BRANCH =~ /-ubi8$/ || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /-ubi8$/ || $UBI_PIPELINE == "true"'
      when: manual
      allow_failure: true
      variables:
        GITLAB_VERSION: "${CI_COMMIT_REF_SLUG}-ubi8"
    # For FIPS pipelines, the images has a `-fips` suffix
    - if: '$CI_COMMIT_BRANCH =~ /-fips$/ || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /-fips$/ || $FIPS_PIPELINE == "true"'
      when: manual
      allow_failure: true
      variables:
        GITLAB_VERSION: "${CI_COMMIT_REF_SLUG}-fips"
    - if: '$CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME'
      when: manual
      allow_failure: true
      variables:
        GITLAB_VERSION: "${CI_COMMIT_REF_SLUG}"
    - when: never

final-images-listing:
  extends: .job-base
  stage: final-list
  script:
    - '[ -f artifacts/container_versions/debian.txt ] && echo "debian.txt - ${DEBIAN_IMAGE}"'
    - '[ -f artifacts/container_versions/alpine.txt ] && echo "alpine.txt - ${ALPINE_IMAGE}"'
    - '[ -f artifacts/container_versions/ubi-minimal.txt ] && echo "ubi-minimal.txt - ${UBI_IMAGE}"'
    - for i in artifacts/final/* ; do echo "${i} - $(cat ${i})" ; done
  needs: !reference [.final_images]
  rules:
    # Skipped for auto-deploy branches via workflow:rules
    # Skip on deps pipeline as the final image listing is useless there
    - !reference [.except-deps, rules]
  artifacts:
    paths:
      - artifacts/final/

image-metrics:
  # intentionally not using .job-base
  stage: final-list
  image: ${DEPENDENCY_PROXY}${ALPINE_IMAGE}
  script:
    - apk add skopeo jq
    - . build-scripts/metrics.sh
    - generate_image_metrics
  needs: !reference [.final_images]
  rules: # NOTE: never runs for auto-deploy branches. See `workflow:rules` above.
    - !reference [.tagless-versionless, rules]
      # Run on any Branch/MR pipeline on .com (where registry is public)
    - if: '($CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME) && $CI_PROJECT_PATH == "gitlab-org/build/CNG"'
  artifacts:
    paths:
      - artifacts/final/
    reports:
      metrics: metrics/image_metrics.txt

.sync-images:
  image: "${DEPENDENCY_PROXY}docker:git"
  retry: 2
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
  services:
    - docker:${DOCKER_VERSION}-dind
  allow_failure: false
  script:
    - apk add skopeo
    - cat artifacts/images/* > image_versions.txt
    - rm -rf artifacts/*
    - sh build-scripts/docker_image_sync.sh image_versions.txt $SYNC_DEST
  artifacts:
    paths:
      - artifacts/

sync-images-gitlab-com:
  extends: .sync-images
  stage: release
  variables:
    SYNC_DEST: gitlab-com
  after_script:
    - if $(echo $CI_COMMIT_TAG | grep -Eq 'v*-ubi8'); then
    -   apk add curl
    -   UBI_IMAGES=$(cat artifacts/gitlab-com-images.txt | xargs printf ', %s')
    -   curl -fS
          --request POST
          --form ref=master
          --form "variables[IMAGES]=${UBI_IMAGES:2}"
          --form "token=${SCANNING_TRIGGER_TOKEN}"
          "${SCANNING_TRIGGER_PIPELINE}"
    - fi
  rules:
    - if: '$CI_COMMIT_TAG && $CI_PROJECT_PATH == "gitlab/charts/components/images"'
      when: manual

sync-images-artifact-registry:
  extends: .sync-images
  stage: sync
  variables:
    SYNC_DEST: artifact-registry
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab/charts/components/images"'

container-scanning:
  stage: container-scanning
  needs: ["final-images-listing"]
  allow_failure: true
  script:
    - images=$(cat artifacts/final/* | xargs printf "$CI_REGISTRY_IMAGE/%s,")
    - if [ "${UBI_PIPELINE}" == "true" -o "${FIPS_PIPELINE}" == "true" ]; then
        images="${images},${UBI_IMAGE}";
        dedupe_model=cng/ubi.yaml;
      else
        images="${images},${DEBIAN_IMAGE}";
        images="${images},${ALPINE_IMAGE}";
        dedupe_model=cng/debian.yaml;
      fi
    - curl -fS
        --request POST
        --form ref=master
        --form "variables[IMAGES]=$images"
        --form "variables[CONTAINER_DEDUPE_MODEL]=$dedupe_model"
        --form "token=${SCANNING_TRIGGER_TOKEN}"
        https://gitlab.com/api/v4/projects/16505542/trigger/pipeline
  rules:
    # run on on Dev for CI_DEFAULT_BRANCH
    - if: '$CI_PROJECT_PATH == "gitlab/charts/components/images" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # run for FIPS tag pipelines and scheduled FIPS pipelines
    - !reference [.if_fips_tag]
    - !reference [.if_scheduled_fips_pipeline]
    # allowed to be run manually other FIPS pipelines. Allowing failure so the
    # job is non-blocking. Scheduled FIPS pipeline will be caught by above
    # rule. We can't use !reference tag here because we want to change when and
    # allow_failure
    - if: '$CI_COMMIT_REF_NAME =~ /-fips$/ || $FIPS_PIPELINE == "true" '
      when: manual
      allow_failure: true

.trigger_alternate_pipeline:
  stage: prepare
  trigger:
    include: '.gitlab-ci.yml'
    strategy: depend
  needs: []
  # Because allow_failure decides whether a manual job is blocking or not,
  # we have to set it to true, unfortunately.
  # https://gitlab.com/groups/gitlab-org/-/epics/6788
  allow_failure: true
  rules:
    - if: '$DEPS_PIPELINE == "true" || $CE_PIPELINE'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /-ubi8$/ || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /-ubi8$/ || $UBI_PIPELINE == "true"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /-fips$/ || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /-fips$/ || $FIPS_PIPELINE == "true"'
      when: never

ubi_pipeline:
  extends:
    - .trigger_alternate_pipeline
  rules:
    - !reference [.trigger_alternate_pipeline, rules]
    - if: '$CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME'
      when: manual
      variables:
        UBI_PIPELINE: 'true'

fips_pipeline:
  extends:
    - .trigger_alternate_pipeline
  rules:
    - !reference [.trigger_alternate_pipeline, rules]
    - if: '$CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME'
      when: manual
      variables:
        FIPS_PIPELINE: 'true'
