ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG GITLAB_DATA=/var/opt/gitlab
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-rails" \
      name="GitLab Rails" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Rails container for GitLab." \
      description="Rails container for GitLab."

RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 \
            libicu tzdata uuid \
            libpng libjpeg-turbo zlib libtiff shadow-utils \
    && microdnf clean all \
    && adduser -m ${GITLAB_USER} -u ${UID} \
    && mkdir -p ${GITLAB_DATA}/{.upgrade-status,data,repo,config} \
    && chown -R ${UID}:0 ${GITLAB_DATA} \
    && chmod -R ug+rwX,o-rwx ${GITLAB_DATA}/repo \
    && chmod -R ug-s ${GITLAB_DATA}/repo \
    # remove shadow-utils within the same layer
    && microdnf remove shadow-utils \
    && microdnf clean all

ADD gitlab-rails-ee.tar.gz /
ADD gitlab-gomplate.tar.gz /
ADD gitlab-exiftool.tar.gz /

COPY scripts/ /scripts

RUN chown -R ${UID}:0 /scripts /srv/gitlab /home/${GITLAB_USER} \
    && chmod o-w /scripts/lib /srv/gitlab /scripts/lib/checks \
    && chmod -R g=u /scripts /home/${GITLAB_USER} /srv/gitlab \
    && mv /srv/gitlab/log/ /var/log/gitlab/ \
    && ln -s /var/log/gitlab /srv/gitlab/log \
    && cd /srv/gitlab \
    && mkdir -p public/uploads \
    && chown -R ${UID}:0 public/uploads \
    && chmod 0700 public/uploads \
    && chmod o-rwx config/database.yml \
    && chmod 0600 config/secrets.yml \
    && chmod -R u+rwX builds/ shared/artifacts/ \
    && chmod -R ug+rwX shared/pages/ \
    && mkdir /home/git/gitlab-shell \
    && chown ${UID}:0 /home/git/gitlab-shell \
    && chmod -R g=u /home/git/gitlab-shell \
    && ln -s /srv/gitlab/GITLAB_SHELL_VERSION /home/git/gitlab-shell/VERSION \
    && sed -e '/host: localhost/d' -e '/port: 80/d' -i config/gitlab.yml \
    && sed -e "s/# user:.*/user: ${GITLAB_USER}/" -e "s:/home/git/repositories:${GITLAB_DATA}/repo:" -i config/gitlab.yml \
    && ldconfig

ENV RAILS_ENV=production
ENV EXECJS_RUNTIME=Disabled
ENV CONFIG_TEMPLATE_DIRECTORY=/srv/gitlab/config
ENV UPGRADE_STATUS_DIR=${GITLAB_DATA}/.upgrade-status

# Set the path that bootsnap will use for cache
ENV BOOTSNAP_CACHE_DIR=/srv/gitlab/bootsnap
# Generate bootsnap cache
RUN echo "Generating bootsnap cache"; \
    # ubi-minimal does not have su. This is messy. See install / remove outputs.
    microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 util-linux && \
    cd /srv/gitlab && \
    su ${GITLAB_USER} -c "mkdir ${BOOTSNAP_CACHE_DIR}"  && \
    su ${GITLAB_USER} -c "ENABLE_BOOTSNAP=1 BOOTSNAP_CACHE_DIR=${BOOTSNAP_CACHE_DIR} bin/rake about" && \
    chown -R ${UID}:0 ${BOOTSNAP_CACHE_DIR} && \
    chmod -R g=u ${BOOTSNAP_CACHE_DIR} && \
    # Remove util-linux and the pile added for it.
    microdnf remove util-linux pam libutempter libtirpc libpwquality libnsl2 libfdisk cracklib cracklib-dicts && \
    du -hs ${BOOTSNAP_CACHE_DIR} ;
# exit code of this command will be that of `du`

VOLUME ${GITLAB_DATA}


