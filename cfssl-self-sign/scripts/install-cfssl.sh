#!/bin/sh
# install-cfssl.sh
set -e

# Version & SHA2 of the checksums list
CFSSL_VERSION=${CFSSL_VERSION:-1.6.1}
CFSSL_CHECKSUM_SHA256="89e600cd5203a025f8b47c6cd5abb9a74b06e3c7f7f7dd3f5b2a00975b15a491"
CFSSL_SOURCE_CHECKSUM="6795ee399b54a3e560768e8bbe742cfb4810e1bcac1cf8a2d99f7e491f66e109"
# Download and install CFSSL from https://github.com/cloudflare/cfssl/releases
CFSSL_PKG_URL="https://github.com/cloudflare/cfssl/releases/download/v${CFSSL_VERSION}"
CFSSL_INSTALL_URL="github.com/cloudflare/cfssl/cmd"
CFSSL_LICENSE="https://raw.githubusercontent.com/cloudflare/cfssl/v${CFSSL_VERSION}/LICENSE"
# we want linux_amd64 by default
CFSSL_PLATFORM=${CFSSL_PLATFORM:-linux_amd64}
# /usr/local/bin is in PATH for Alpine & RedHat UBI
CFSSL_BIN=${CFSSL_BIN:-/usr/local/bin}
TARGETARCH=${TARGETARCH:-amd64}
UBI_BUILD_IMAGE=${UBI_BUILD_IMAGE:-false}
CFSSL_ITEMS="cfssl cfssljson"

CWD=`pwd`
WORK_DIR=$(mktemp -d)
cd $WORK_DIR
if [ $TARGETARCH = "amd64" ]; then
  # Fetch CHECKSUMS
  echo "Fecthing CHECKSUMS for ${CFSSL_VERSION}"
  CHECKSUM_FILE="cfssl_${CFSSL_VERSION}_checksums.txt"
  curl --retry 6 -fJLO "${CFSSL_PKG_URL}/${CHECKSUM_FILE}"

  echo "Fetching items: ${CFSSL_ITEMS}"
  for item in ${CFSSL_ITEMS} ; do
    ITEM_PATH="${item}_${CFSSL_VERSION}_${CFSSL_PLATFORM}"
    ITEM_URL="${CFSSL_PKG_URL}/${ITEM_PATH}"
    echo "Fetching '${item}' from '${ITEM_URL}'"
    curl --retry 6 -fJLO  "$ITEM_URL"
    grep ${ITEM_PATH} ${CHECKSUM_FILE} >> checksums.txt
  done
else
  if [ "$UBI_BUILD_IMAGE" = false ]; then
    apk add --no-cache git make musl-dev go gcc
    export GOROOT=/usr/lib/go
  else
    microdnf install -y go
    microdnf clean all
    export GOROOT=/usr/lib/golang
  fi
  export GOPATH=/go
  export PATH=$GOPATH/bin:$PATH

  echo "Creating checksums file for ${CFSSL_VERSION}"
  echo "${CFSSL_SOURCE_CHECKSUM}  cfssl-v${CFSSL_VERSION}.zip" >> checksums.txt

  echo "Installing items: ${CFSSL_ITEMS}"
  curl -L http://github.com/cloudflare/cfssl/zipball/v${CFSSL_VERSION}/ -o cfssl-v${CFSSL_VERSION}.zip
  unzip cfssl-v${CFSSL_VERSION}.zip
  SOURCE=$(find -maxdepth 1 -name cloudflare-cfssl-* -print)
  cd $SOURCE
  for item in ${CFSSL_ITEMS} ; do
    echo "Install '${item}' in '${GOPATH}/bin'"
    make bin/${item}
    make install-${item}
  done
  cd $WORK_DIR
fi
echo "Verifying checksums"
sha256sum -c checksums.txt
if [ $TARGETARCH = "amd64" ]; then
  for item in ${CFSSL_ITEMS} ; do
    echo "Placing '${item}' in '${CFSSL_BIN}'"
    mv "${item}_${CFSSL_VERSION}_${CFSSL_PLATFORM}" ${CFSSL_BIN}/${item}
    chmod +x ${CFSSL_BIN}/${item}
  done
else
  for item in ${CFSSL_ITEMS} ; do
    echo "Placing '${item}' in '${CFSSL_BIN}'"
    mv $GOPATH/bin/${item} ${CFSSL_BIN}/${item}
  done
fi
echo "Fetching LICENSE"
curl -fJLo ${CFSSL_BIN}/cfssl.LICENSE "${CFSSL_LICENSE}"

cd $CWD
rm -rf $WORK_DIR
