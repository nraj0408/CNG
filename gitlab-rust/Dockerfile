ARG DEBIAN_IMAGE=debian:bullseye-slim

FROM ${DEBIAN_IMAGE} as base

ARG RUST_VERSION=1.65.0
ARG RUST_PLATFORM=x86_64-unknown-linux-gnu
ARG BUILD_DIR=/tmp/build
ARG TARGETARCH

FROM base as base-amd64
ARG RUST_PLATFORM=x86_64-unknown-linux-gnu
FROM base as base-arm64
ARG RUST_PLATFORM=aarch64-unknown-linux-gnu
FROM base-$TARGETARCH
ENV RUST_URL="https://static.rust-lang.org/dist/rust-$RUST_VERSION-$RUST_PLATFORM.tar.gz"

# Install Rust
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        ca-certificates \
    && ln -s /etc/ssl/certs/ca-certificates.crt /usr/lib/ssl/cert.pem \
    && mkdir ${BUILD_DIR} \
    && cd ${BUILD_DIR} \
    && curl --retry 6 -sfo rust.tar.gz ${RUST_URL} \
    && tar -xzf rust.tar.gz \
    && rust-$RUST_VERSION-$RUST_PLATFORM/install.sh --components=rustc,cargo,rust-std-$RUST_PLATFORM --destdir=/assets \
    && rm rust.tar.gz \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf ${BUILD_DIR}
